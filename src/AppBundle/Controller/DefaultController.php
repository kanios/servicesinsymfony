<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $user = new \stdClass();
        $user->firstName = 'Jasio';
        $user->lastName = 'Kowalski';
        $user->password = 'passwd';

        $userValidation = $this->get('app.user_validation');
        $html = 'IS VALID: ' . json_encode($userValidation->isValid($user)) . '<br/>';

        return $this->render('default/index.html.twig', [
            'html' => $html
        ]);
    }
}
