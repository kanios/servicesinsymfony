<?php

namespace AppBundle\Service\Validation;

interface ValidatorInterface
{
    public function validate($user);
}