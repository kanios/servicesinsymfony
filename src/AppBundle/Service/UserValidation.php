<?php

namespace AppBundle\Service;

use AppBundle\Service\Validation\ValidatorInterface;

class UserValidation implements ValidationInterface
{
    /**
     * @var ValidationInterface
     */
    private $userValidator;

    public function __construct(ValidatorInterface $userValidator)
    {
        $this->userValidator = $userValidator;
    }

    public function isValid($user)
    {
        $errors = $this->userValidator->validate($user);

        return count($errors) == 0;
    }
}
