<?php

namespace AppBundle\Service;

interface ValidationInterface
{
    public function isValid($user);
}
